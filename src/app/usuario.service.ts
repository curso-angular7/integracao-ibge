import { Injectable } from '@angular/core';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor() { }

  public getUsuario(): Usuario{
    let usuario = new Usuario()
    usuario.nome = "Nataniel"
    usuario.email = "nataniel.paiva@gmail.com"

    return usuario
  }

  public listaUsuario():Usuario[]{
    return [
      {
        nome:"Nataniel",
        email:"nataniel.paiva@gmail.com"
      },
      {
        nome:"Maria",
        email:"maria.paiva@gmail.com"
      },
      {
        nome:"Laura",
        email:"laura.paiva@gmail.com"
      },
      {
        nome:"Jéssica",
        email:"jessica.paiva@gmail.com"
      },
    ]
  }

}
